Source: cube2
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Bruno "Fuddl" Kleinert <fuddl@debian.org>,
 Vincent Cheng <vcheng@debian.org>,
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libenet-dev,
 libsdl2-image-dev,
 libsdl2-mixer-dev,
 libsdl2-dev,
 zlib1g-dev
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/games-team/cube2.git
Vcs-Browser: https://salsa.debian.org/games-team/cube2
Homepage: http://cubeengine.com

Package: cube2
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 cube2-data
Description: 3D first-person shooter game engine
 Cube2 is a networked 3D first-person shooter game engine. It supports modern
 graphic effects and conveys a sense of fast-paced oldschool gameplay.
 .
 The game client also works as the map editor. It is even possible to create
 and edit a map together with other people over a network connection.
 .
 The effects of Cube2 might be considered unsuitable for children.
 .
 This package installs the game client and map editor of the Cube2 engine.
 You need to install a content package like cube2-data if you want to play a
 game.

Package: cube2-server
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 cube2
Description: standalone server for Cube2 based games
 Cube2 is a networked 3D first-person shooter game engine. It supports modern
 graphic effects and conveys a sense of fast-paced oldschool gameplay.
 .
 This package installs the standalone server for Cube2 based games.
