#!/bin/sh

RELEASEDATE=20201229

svn co -r \{$((${RELEASEDATE} + 1))\} \
     https://svn.code.sf.net/p/sauerbraten/code/src \
     cube2-0.0.${RELEASEDATE}+dfsg

find cube2-0.0.${RELEASEDATE}+dfsg -type d -name '.svn' \
	-exec rm -rf "{}" \;
rm -rf cube2-0.0.${RELEASEDATE}+dfsg/vcpp
rm -rf cube2-0.0.${RELEASEDATE}+dfsg/xcode
rm -rf cube2-0.0.${RELEASEDATE}+dfsg/lib
rm -rf cube2-0.0.${RELEASEDATE}+dfsg/lib64
rm -rf cube2-0.0.${RELEASEDATE}+dfsg/enet
tar caf cube2_0.0.${RELEASEDATE}+dfsg.orig.tar.xz \
	cube2-0.0.${RELEASEDATE}+dfsg

rm -rf cube2-0.0.${RELEASEDATE}+dfsg
