Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Cube2
Source: https://svn.code.sf.net/p/sauerbraten/code/src

Files: *
Copyright: 2001-2020, Wouter van Oortmerssen, Lee Salzman, Mike Dysart,
                      Robert Pointon, Quinton Reeves
           1995-2005, Jean-loup Gailly and Mark Adler
License: zlib

Files: include/SDL_*
Copyright: 1997-2009, Sam Lantinga
License: LGPL-2.1+

Files: debian/*
Copyright: 2007-2011, Bruno "Fuddl" Kleinert <fuddl@debian.org>
           2014,      Vincent Cheng <vcheng@debian.org>
           2014-2021, Markus Koschany <apo@debian.org>
License: zlib

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: LGPL-2.1+
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

